# Personal Access Restriction 8.x

## Introduction

Personal Access Restriction module provides the ability to restrict access to
particular Node or Taxonomy Term by UID or User role.
Also, it provides the ability to select the type of response
("Access denied" or "Page not found").

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/par

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/par


## Getting Started

 1. Install Personal Access Restriction module
 (<code>composer require drupal/par</code>)
 2. Enable Personal Access Restriction module
 3. Set permissions for 'Configure all Personal Access Restrictions',
    'Manage Personal Access Restriction' and 'View restricted pages' at
    /admin/people/permissions
 4. Add your access restrictions for any Node ar Taxonomy term
 on their add/edit pages
 (you will see the 'Personal Access Restriction' collapsed container).
 5. Fill the  **User ID** field to restrict access to an entity for particular
    users. You can set multiple UIDs separated by whitespace.
 6. Fill the **Roles** field in case if you want to restrict access to an
    entity for all users with some role.
 7. In the **How to show the page?** field you can set the access restriction
    type for this entity. Also, there is a default variant which you can use
    in case if you want just to select users or roles for further access
    restriction but do not want to restrict access right now.
 8. You can see a list of all restrictions at /admin/config/par. On this page
    you can filter access restrictions
    by Entity type, Entity ID, UID, User role and action.
 9. If you want to provide the ability to see restricted entities,
 you can add 'View restricted pages' permission to some User role at
    /admin/people/permissions#module-par.
    Users with the 'Administrator' role will see pages
    with restricted access in any case.


## Requirements

No special requirements.


## Installation

 * Install as you would normally install a contributed Drupal module.\
   See: https://www.drupal.org/node/895232 for further information.

 * If you are using Composer you can install this module by the command:\
   <code>composer require drupal/par</code>


## Configuration

 * Configure the user permissions in Administration » People » Permissions:

   - Configure all Personal Access Restrictions (Personal Access Restriction)
   - Manage Personal Access Restriction (Personal Access Restriction)
   - View restricted pages (Personal Access Restriction)

     The top-level administration categories require this permission to be
     accessible.


## Maintainers

 * Roman (rsych) - https://www.drupal.org/u/rsych

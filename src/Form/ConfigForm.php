<?php

namespace Drupal\par\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\par\ParService;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigForm for setting access options for an Entity.
 */
class ConfigForm extends FormBase {

  /**
   * Drupal\par\ParService definition.
   *
   * @var \Drupal\par\ParService
   */
  protected $par;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(ParService $par,
    Connection $database,
    AccountInterface $currentUser,
    EntityTypeManagerInterface $entityTypeManager,
    RendererInterface $renderer) {
    $this->par = $par;
    $this->database = $database;
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('par.default'),
      $container->get('database'),
      $container->get('current_user'),
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'records_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form,
    FormStateInterface $form_state,
    $entity_type = 'all',
    $entity_id = 0,
    $uid = 0,
    $role = 'all',
    $action = 'all') {
    $rows = [];
    $par_service = $this->par;
    // Prepare options for the Roles field.
    $user_roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    // Users with Administrator role will always have access.
    unset($user_roles['administrator']);
    foreach ($user_roles as $user_role) {
      $roles_options[$user_role->id()] = $user_role->label();
    }

    $form['filter'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filter options'),
    ];

    $form['filter']['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity type'),
      '#options' => [
        'node' => $this->t('Node'),
        'taxonomy_term' => $this->t('Taxonomy term'),
      ],
      '#empty_option' => $this->t('All types'),
      '#default_value' => $entity_type,
    ];

    $form['filter']['entity_id'] = [
      '#type' => 'number',
      '#title' => $this->t('Entity ID'),
      '#min' => 1,
    ];
    if ($entity_id) {
      $form['filter']['entity_id']['#default_value'] = $entity_id;
    }

    $form['filter']['uid'] = [
      '#type' => 'number',
      '#title' => $this->t('User ID'),
      '#min' => 1,
    ];
    if ($uid) {
      $form['filter']['uid']['#default_value'] = $uid;
    }

    $form['filter']['role'] = [
      '#type' => 'select',
      '#title' => $this->t('User roles'),
      '#options' => $roles_options,
      '#empty_option' => $this->t('All roles'),
      '#default_value' => $role,
    ];

    $allowed = ParService::PAR_ACCESS_ALLOWED;
    $denied = ParService::PAR_ACCESS_DENIED;
    $not_found = ParService::PAR_NOT_FOUND;

    $form['filter']['action'] = [
      '#type' => 'select',
      '#title' => $this->t('Actions'),
      '#options' => [
        $allowed => $par_service->getAccessLabelById($allowed),
        $denied => $par_service->getAccessLabelById($denied),
        $not_found => $par_service->getAccessLabelById($not_found),
      ],
      '#empty_option' => $this->t('All actions'),
      '#default_value' => $action,
    ];

    $form['filter']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];

    // Prepare conditions for request.
    $entity_type = ($entity_type != 'all') ? $entity_type : '';
    $role = ($role != 'all') ? $role : '';
    $roles = [$role];
    $action = ($action != 'all') ? $action : '';

    // Get records.
    $records = $par_service->getRecords(
      $entity_type,
      $entity_id,
      $uid,
      $roles,
      $action
    );
    foreach ($records as $record) {
      $key = $record->entity_type . '__' . $record->entity_id;
      // Output User ID as link, if possible.
      $user_is_link = FALSE;
      if ($record->uid) {
        $user_link = [
          '#type' => 'link',
          '#title' => $record->uid,
          '#url' => Url::fromRoute('entity.user.canonical', [
            'user' => $record->uid,
          ]),
        ];
        $user_link_rendered = $this->renderer->render($user_link);
        $user_is_link = TRUE;
      }
      if (empty($rows[$key])) {
        // Output Entity ID as link, if possible.
        $entity_is_link = FALSE;
        if (in_array($record->entity_type, ['node', 'taxonomy_term'])) {
          $view_page_route = 'entity.' . $record->entity_type . '.canonical';
          $route_params = [
            $record->entity_type => $record->entity_id,
          ];
          $entity_link = [
            '#type' => 'link',
            '#title' => $record->entity_id,
            '#url' => Url::fromRoute($view_page_route, $route_params),
          ];
          $entity_link = $this->renderer->render($entity_link);
          $entity_is_link = TRUE;
        }

        $rows[$key] = [
          'entity_type' => $record->entity_type,
          'entity_id' => $entity_is_link ? $entity_link : $record->entity_id,
          'uids' => $user_is_link ? [$user_link_rendered] : [$record->uid],
          'roles' => !empty($record->role) ? [$record->role] : [],
          'action' => $par_service->getAccessLabelById($record->action),
        ];
      }
      else {
        if (!in_array($record->uid, $rows[$key]['uids']) && $record->uid) {
          $rows[$key]['uids'][] = $user_is_link ? $user_link_rendered : $record->uid;
        }
        if (!in_array($record->role, $rows[$key]['roles']) && !empty($record->role)) {
          $rows[$key]['roles'][] = $roles_options[$record->role];
        }
      }
    }

    foreach ($rows as $key => $row) {
      // Need to organize UIDs list in this way for correct rendering.
      $users_string = '';
      $users_params = [];
      foreach ($row['uids'] as $uid_key => $uid_item) {
        $users_string .= '@' . $uid_key;
        if ($uid_item != end($row['uids'])) {
          $users_string .= ', ';
        }
        $users_params['@' . $uid_key] = $uid_item;
      }
      $rows[$key]['uids'] = new FormattableMarkup($users_string, $users_params);
      $rows[$key]['roles'] = implode(', ', $row['roles']);
    }

    $form['table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Entity type'),
        $this->t('Entity ID'),
        $this->t('User IDs'),
        $this->t('User roles'),
        $this->t('Action'),
      ],
      '#rows' => $rows,
      '#empty' => $this->t('No results'),
      '#sticky' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $entity_type = $values['entity_type'] ? $values['entity_type'] : 'all';
    $entity_id = $values['entity_id'] ? $values['entity_id'] : 0;
    $uid = $values['uid'] ? $values['uid'] : 0;
    $role = $values['role'] ? $values['role'] : 'all';
    $action = $values['action'] ? $values['action'] : 'all';

    $form_state->setRedirect('par.config', [
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
      'uid' => $uid,
      'role' => $role,
      'action' => $action,
    ]);
  }

}

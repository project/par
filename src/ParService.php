<?php

namespace Drupal\par;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ParService provides main functions for setting access to entities.
 */
class ParService {

  use StringTranslationTrait;

  /**
   * Constants for access statuses marking.
   */
  const PAR_ACCESS_ALLOWED = 0;
  const PAR_ACCESS_DENIED = 1;
  const PAR_NOT_FOUND = 2;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Create dependency injection for class.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container for dependency injections.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a new DatabaseLockBackend.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The user who is currently logged in.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager for control operations with entities.
   */
  public function __construct(Connection $database, AccountInterface $currentUser, EntityTypeManagerInterface $entityTypeManager) {
    $this->database = $database;
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Writes new record to the par table.
   *
   * @param string $entity_type
   *   Type of selected entity.
   * @param int $entity_id
   *   ID of selected entity.
   * @param int $action
   *   Type of action to do with selected entity.
   * @param array $uids
   *   IDs of users whose access to selected entity will be affected.
   * @param array $roles
   *   User roles whole access to selected entity will be affected.
   */
  public function addRecords($entity_type, $entity_id, $action = 0, array $uids = [], array $roles = []) {
    $fields = [
      'entity_type' => $entity_type,
      'entity_id' => $entity_id,
      'action' => $action,
    ];
    foreach ($uids as $uid) {
      if ($uid) {
        $this->database->insert('par')
          ->fields($fields + [
            'uid' => (int) $uid,
          ])
          ->execute();
      }
    }
    foreach ($roles as $role) {
      if ($role) {
        $this->database->insert('par')
          ->fields($fields + [
            'role' => $role,
          ])
          ->execute();
      }
    }
  }

  /**
   * Update existing records for certain content.
   *
   * @param string $entity_type
   *   Type of selected entity.
   * @param int $entity_id
   *   ID of selected entity.
   * @param int $action
   *   Type of action to do with selected entity.
   * @param array $uids
   *   IDs of users whose access to selected entity will be affected.
   * @param array $roles
   *   User roles whole access to selected entity will be affected.
   */
  public function updateRecords($entity_type, $entity_id, $action = 0, array $uids = [], array $roles = []) {
    $records = $this->database->select('par', 'p')
      ->fields('p')
      ->condition('entity_type', $entity_type)
      ->condition('entity_id', $entity_id)
      ->execute()
      ->fetchAll();

    if ($records) {
      $this->database->delete('par')
        ->condition('entity_type', $entity_type)
        ->condition('entity_id', $entity_id)
        ->execute();
    }

    $this->addRecords($entity_type, $entity_id, $action, $uids, $roles);
  }

  /**
   * Get Records by Entity ID.
   *
   * @param int $entity_id
   *   Entity ID for checking accesses by users.
   *
   * @return mixed
   *   Returns result of the Database selection.
   */
  public function getRecordsByEntityId($entity_id) {
    return $this->database->select('par', 'p')
      ->fields('p')
      ->condition('entity_id', $entity_id)
      ->execute()
      ->fetchAll();
  }

  /**
   * Get records by User ID.
   *
   * @param mixed $uid
   *   User ID for checking accesses to entities.
   *
   * @return mixed
   *   Returns result of the Database selection function.
   */
  public function getRecordsByUserId($uid = NULL) {
    if (!$uid) {
      $uid = $this->currentUser->id();
    }

    return $this->database->select('par', 'p')
      ->fields('p')
      ->condition('uid', $uid)
      ->execute()
      ->fetchAll();
  }

  /**
   * Get Records using any params.
   *
   * @param string $entity_type
   *   Entity type for checking access records.
   * @param int $entity_id
   *   Entity ID for checking access records.
   * @param int $uid
   *   User ID for checking access records.
   * @param array $roles
   *   User roles for checking access records.
   * @param string $action
   *   Action type for checking access records.
   *
   * @return mixed
   *   Returns result of the Database selection.
   */
  public function getRecords($entity_type = '', $entity_id = 0, $uid = NULL, $roles = [], $action = '') {
    $query = $this->database->select('par', 'p')
      ->fields('p');

    if ($entity_id) {
      $query->condition('entity_id', $entity_id);
    }
    if ($entity_type) {
      $query->condition('entity_type', $entity_type);
    }

    $orGroup = $query->orConditionGroup();
    if ($uid) {
      $orGroup->condition('uid', $uid);
    }
    if (!empty($roles)) {
      foreach ($roles as $role) {
        if (!empty($role)) {
          $orGroup->condition('role', $role);
        }
      }
    }

    if ($action) {
      $query->condition('action', $action);
    }

    if ($orGroup->count() > 0) {
      $query->condition($orGroup);
    }

    return $query->execute()->fetchAll();
  }

  /**
   * Returns access content status for a particular entity item.
   *
   * @param string $entity_type
   *   Type of the entity whose access is being checked.
   * @param int $entity_id
   *   ID of the entity whose access is being checked.
   * @param int $uid
   *   User ID for checking access to selected entity.
   *
   * @return int
   *   Returns access type to selected entity.
   */
  public function getContentAccessStatus($entity_type, $entity_id, $uid = 0) {
    $user = $uid ? $this->entityTypeManager->getStorage('user')->load($uid) : $this->currentUser;
    $uid = $user->id();
    $roles = ($uid == 0) ? ['anonymous'] : $user->getRoles();
    $is_admin = in_array('administrator', $roles);
    $has_view_permission = $user->hasPermission('view restricted pages');
    if ($is_admin || $has_view_permission) {
      return self::PAR_ACCESS_ALLOWED;
    }

    $records = $this->getRecords($entity_type, $entity_id, $uid, $roles);
    if (!$records) {
      return self::PAR_ACCESS_ALLOWED;
    }
    $record = reset($records);

    return $record->action;
  }

  /**
   * Returns label of the access ID code.
   *
   * @param int $id
   *   ID of the access type.
   *
   * @return mixed
   *   Label of the selected access type.
   */
  public function getAccessLabelById($id) {
    $ids = [
      self::PAR_ACCESS_ALLOWED => $this->t('Access allowed'),
      self::PAR_ACCESS_DENIED => $this->t('Access denied'),
      self::PAR_NOT_FOUND => $this->t('Page not found'),
    ];

    return $ids[$id];
  }

}
